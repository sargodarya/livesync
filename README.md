# LiveSync Demo

## Installing

Clone the repo and then run `npm i` in the root.

## Running the service

Run `node index.js` and it will launch a server listening on http://localhost:3006 by default.

This will expose 2 endpoints:

- `/live/host` will open the host view where you can start a new session for the host.

- `/live` will host the client code which allows entering a given code from `/live/host`
