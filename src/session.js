'use strict';

class Session {

  /**
   * Creates given session
   * @param {String} id
   * @param {String} privilegeToken
   * @param {String} options
   */
  constructor(id, privilegeToken, options) {
    this.id = id;
    this.privilegeToken = privilegeToken;

    /** @type {Array} */
    this.participants = [];

    /** @type {Array} */
    this.js = options.js || [];

    /** @type {Array} */
    this.css = options.css || [];

    /** @type {Array} */
    this.messageHistory = [];

    console.log(this.id + ' - Session created with ', this.js, this.css);
  }

  broadcastCommonScreenSize(participant) {
    var lowestWidth;
    var lowestHeight;

    this.participants.forEach(participant => {
      lowestWidth = (!lowestWidth || (participant.screenWidth && participant.screenWidth < lowestWidth)) ? participant.screenWidth : lowestWidth;
      lowestHeight = (!lowestHeight || (participant.screenHeight && participant.screenHeight < lowestHeight)) ? participant.screenHeight : lowestHeight;
    });

    participant.to(this.id).emit('event', {
      type: 'resize',
      width: lowestWidth,
      height: lowestHeight
    })
  }

  /**
   * Returns true if the given participant wasn't yet
   * in this session.
   *
   * @param {Socket} participant
   * @param {String} privilegeToken
   * @returns {Boolean}
   */
  join(participant, privilegeToken, callback) {
    if (this.participants.indexOf(participant) === -1) {
      participant.join(this.id);
      this.participants.push(participant);

      participant.isAuthorized = privilegeToken === this.privilegeToken;

      console.log(this.id + ' - User authorized: ' + participant.isAuthorized);

      // No errors happened, accept the connection
      callback({
        accepted: true,
        js: this.js,
        css: this.css
      });

      if (participant.isAuthorized) {
        // Setup events to listen to from the authorized user
        participant.on('message', (msg) => {
          console.log(this.id + ' - Received message: ' + msg.f);
          this.messageHistory.push(msg);
          participant.to(this.id).emit('message', msg);
        });

        participant.on('event', (evt) => {
          participant.to(this.id).emit('event', evt);
        });
      } else {
        // Replay message history to new participant
        setTimeout(() => {
          console.log(this.id + ' - Replaying history to new participant');
          this.messageHistory.forEach(msg => participant.emit('message', msg));
        }, 1000);
      }

      participant.on('resize', (evt) => {
        participant.screenWidth = evt.width;
        participant.screenHeight = evt.height;

        this.broadcastCommonScreenSize(participant);
      });

      return true;
    }

    callback({
      error: 'Konnte Session mit Code ' + sessionData.sessionId + ' nicht beitreten.'
    });
    return false;
  }

  /**
   * Ends the session if the user is authorized otherwise removes participant
   * from session and rebroadcasts a few things.
   */
  end(participant) {
    console.log(this.id + ' - Removing participant from session');
    this.participants.splice(this.participants.indexOf(participant), 1);
    this.broadcastCommonScreenSize(participant);

    if(!participant.isAuthorized) {
      return false;
    }

    // check if there still is an authorized user so we don't close the session
    // too early
    if (this.participants.filter(participant => participant.isAuthorized).length > 0) {
      return false;
    }

    // Loop through each participant and end the session
    this.participants.forEach(participant => {
      participant.emit('end');
      participant.disconnect();
    });

    console.log(this.id + ' - Session ended');

    return true;
  }
}

module.exports = Session;