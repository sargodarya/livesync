'use strict';

const randtoken = require('rand-token');
const Session = require('./session');

const SESSION_LIMIT = 999999999;
const SESSION_TOKEN_LENGTH = 9;
const SESSION_TOKEN_CHARS = '0123456789'

const PRIVILEGE_TOKEN_LENGTH = 8;

class SessionManager {

  constructor() {
    this.sessions = {};
  }

  /**
   *
   */
  createSession(options) {
    const sessionId = randtoken.generate(SESSION_TOKEN_LENGTH, SESSION_TOKEN_CHARS);
    const privilegeToken = randtoken.uid(PRIVILEGE_TOKEN_LENGTH);

    this.sessions[sessionId] = new Session(sessionId, privilegeToken, options);

    return {
      sessionId,
      privilegeToken
    };
  }

  /**
   *
   */
  joinSession(connection, sessionId, privilegeToken, callback) {
	  if (this.sessions[sessionId]) {
      return this.sessions[sessionId].join(connection, privilegeToken, callback);
    }

    console.log(sessionId + ' - not a valid session.');
    callback({
      error: 'Konnte Sitzung mit ID ' + sessionId + ' nicht beitreten.'
    });

    return false;
  }

  /**
   *
   */
  close(sessionId, participant) {
    if (this.sessions[sessionId]) {
      if (this.sessions[sessionId].end(participant)) {
        delete this.sessions[sessionId];
      }
    }
  }

}

module.exports = SessionManager;