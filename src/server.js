/* global process */

'use strict';

const express = require('express');
const http = require('http');
const socketio = require('socket.io');
const bodyParser = require('body-parser');
const SessionManager = require('./session-manager');
const util = require('util');
const filesize = require('filesize');

module.exports = class Server {
  constructor() {
    this.app = express();
    this.server = http.Server(this.app);
    this.io = socketio(this.server);

    /** @type {SessionManager} */
    this.sessionManager = new SessionManager;

    this.io.on('connection', this.handleSocketConnection.bind(this));

    this.app.use(bodyParser.json());
    this.app.use('/live', express.static('public'));
    this.app.set('view engine', 'jade');

    this.setupRoutes();

    // Setup memory watcher
    setInterval(_ => {
      var mem = process.memoryUsage();
      console.log('Current memory usage: ' + filesize(mem.heapUsed) + '/' + filesize(mem.heapTotal));
    }, 60 * 1000);
  }

  handleSocketConnection(conn) {
    // 5 second timeout handlers
    let timeoutHandler = setTimeout(() => conn.disconnect(), 5000);

    conn.once('responseSession', (sessionData, callback) => {
      clearTimeout(timeoutHandler);

      // If Session Data is invalid close the connection
      if (!sessionData || !sessionData.sessionId) {
        callback({
          error: 'Es wurde kein gültiger Code gesendet'
        });
        return conn.disconnect();
      }

      this.sessionManager.joinSession(conn, sessionData.sessionId, sessionData.privilegeToken, callback);

      conn.on('closeSession', () => {
        this.sessionManager.close(sessionData.sessionId, conn);
      });

      conn.on('disconnect', () => {
        this.sessionManager.close(sessionData.sessionId, conn);
      });
    });

    conn.emit('requestSession');
  }

  /**
   * Sets up specific routes for handling static views
   */
  setupRoutes() {
    this.app.post('/live/session', (req, res) => {
      res.send(this.sessionManager.createSession(req.body));
    });

    this.app.get('/live', (req, res) => {
      res.render('client');
      return res.end();
    });

    /**
     * Serves the host site
     */
    this.app.get('/live/host', (req, res) => {
      res.render('host');
      return res.end();
    });

    /**
     * Serves dummy offers
     */
    this.app.get('/api/requestOffers', (req, res) => {
	    var BLARange = 150;
      var interestRate = 234;
      var interest = 2;
      var providers = ['DiBa', 'Commerzbank', 'Deutsche Bank', 'DKB', 'Sparkasse'];

      var offers = [];
      for (var i = 0; i < 10; i++) {
        offers.push({
          bla: Math.floor(Math.random() * BLARange),
          interestRate: Math.floor(Math.random() * interestRate + 1000),
          interest: (Math.random() * interest).toPrecision(3),
          provider: providers[Math.floor(Math.random() * providers.length)],
          feasability: Math.floor(Math.random() * 3)
        })
      }

      res.send(JSON.stringify({
        parameters: [
          {
            label: 'Annuitätendarlehen',
            type: 'EUR',
            value: 50000
          },
          {
            label: 'Zinsbindung',
            type: 'Jahre',
            value: 10
          },
          {
            label: 'Tilgungswunsch',
            type: '%',
            value: '2,00'
          },
          {
            label: 'Sondertilgung',
            type: '%',
            value: '5,00'
          },
          {
            label: 'KfW Darlehen (124)',
            type: 'EUR',
            value: 50000
          },
          {
            label: 'Zinsbindung',
            type: 'Jahre',
            value: 15
          },
          {
            label: 'Tilgungswunsch',
            type: '%',
            value: '2,21'
          },
          {
            label: 'Tilgungsfrei',
            type: 'Jahre',
            value: 1
          }
        ],
        offers: offers
      }));
    });
  }

	listen(port) {
    this.server.listen(port);
  }
}