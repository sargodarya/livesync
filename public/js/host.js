/* global LiveSync */

(function() {
  'use strict';

  window.module = {};

  document.addEventListener('DOMContentLoaded', function () {
    'use strict';

    var startSessionButton = document.getElementById('start-session');
    var endSessionButton = document.getElementById('end-session');
    var sessionNotRunningContent = document.getElementById('session-not-running');
    var sessionRunningContent = document.getElementById('session-running');
    var sessionIdInput = document.getElementById('session-id');
    var appLink = document.getElementById('app-link');

    /**
     * Switches to Session View
     */
    function switchToSessionMode(err, response) {
      if (err) {
        return err;
      }

      sessionIdInput.value = response.sessionId;
      appLink.innerText = '/live#' + response.sessionId;
      appLink.setAttribute('href', '/live#' + response.sessionId);

      sessionRunningContent.classList.remove('hide');
      sessionNotRunningContent.classList.add('hide');
    }

    /**
     *
     */
    if (startSessionButton) {
      startSessionButton.addEventListener('click', function() {
        LiveSync.requestSession({
          css: [
            '/live/css/application.css',
          ],
          js: [
            '/live/static/bundle.js'
          ]
        }, switchToSessionMode);
      });
    }

    if (endSessionButton) {
      endSessionButton.addEventListener('click', function() {
        LiveSync.endSession(true, function (err, response) {});
      });
    }

    sessionRunningContent.classList.add('hide');
  });

})();