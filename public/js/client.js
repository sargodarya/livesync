/* global LiveSync */

document.addEventListener('DOMContentLoaded', function () {
  'use strict';

  var connectButton = document.getElementById('livesync-connect');
  var sessionIdInput = document.getElementById('livesync-code');
  var presessionContent = document.getElementById('livesync-presession');
  var sessionContent = document.getElementById('livesync-content');
  var errorMessage = document.getElementById('livesync-error');

  /**
   * Switches to Session View
   */
  function switchToSessionMode(response) {
    sessionContent.classList.remove('hide');
    presessionContent.classList.add('hide');
  }

  function connectToService(value) {
    LiveSync.connect(sessionIdInput.value, null, function (err, response) {
      if (err) {
        errorMessage.innerText = err;
        return false;
      } else {
        switchToSessionMode(response);
        return true;
      }
    });
  }

  /**
   *
   */
  if (connectButton) {
    connectButton.addEventListener('click', function() {
      connectToService(sessionIdInput.value);
    });
  }

  if (/^#[0-9]{9}$/.test(location.hash)) {
    sessionIdInput.value = location.hash.replace('#', '');
    connectToService(sessionIdInput.value);
  }

  sessionContent.classList.add('hide');
});