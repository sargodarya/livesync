/* global io, TreeMirror, TreeMirrorClient */

var module = {};

// IE Polyfill
if (!window.location.origin) {
  window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
}

(function (window) {
  'use strict';

  var DrawingMode = {
    POINTER: 0,
    LINE: 1,
    RECT: 2,
    ARROW: 3,
    CIRCLE: 4
  };

  var LiveSync = function () {};

  /**
   * Current connection
   */
  var connection = null;

  /**
   * Respective instance of either a client or host
   * from TreeMirror lib
   */
  var mirrorClient = null;

  var messageHandlers = [];

  /**
   * Reference to the livesync header
   */
  var header = null;

  /**
   * Current drawing mode, defaults to pointer
   */
  var currentDrawingMode = DrawingMode.POINTER;

  /**
   * Handles incoming messages from the socket
   */
  var onMessage = function(msg) {
    var msg = msg;
    if (msg instanceof Array) {
      msg.forEach(function(subMessage) {
        handleMessage(JSON.parse(subMessage));
      });
    } else {
      handleMessage(msg);
    }
  }

  /**
   * Handles incoming events like mouse events
   */
  var onEvent = function (evt) {
    switch (evt.type) {
      case 'mousemove': moveVirtualPointer(evt.x, evt.y); break;

      case 'click': showClickAt(evt.x, evt.y); break;

      case 'scroll': scrollToPos(evt.scrollX, evt.scrollY); break;

      case 'resize': resizeContent(evt.width, evt.height); break;
    }
  }

  // Handles a sub message
  function handleMessage(msg) {
    mirrorClient[msg.f].apply(mirrorClient, msg.args);
  };

  function scrollToPos(x, y) {
    var targetElement = document.getElementById('livesync-content');

    if (targetElement) {
      targetElement.scrollTop = y;
      targetElement.scrollLeft = x;
    }
  }

  /**
   * Displays a click at the given coordinates
   * @param {Number} x
   * @param {Number} y
   */
  function showClickAt(x, y) {
    var clickEl = document.createElement('div');
    clickEl.className = 'click--effect';
    clickEl.style.left = x + 'px';
    clickEl.style.top = y + 'px';
    clickEl.style.position = 'absolute';

    document.body.appendChild(clickEl);

    setTimeout(function () {
      clickEl.remove();
    }, 1000);
  };

  /**
   * Moves a virtual pointer to given coordinates
   * @param {Number} x
   * @param {Number} y
   */
  function moveVirtualPointer(x, y) {
    var el = document.getElementsByClassName('virtual-cursor')[0];
    if (el) {
      el.style.top = y + 'px';
      el.style.left = x + 'px';
    }
  }

  /**
   * Resizes the viewport to given dimensions
   * @param {Number} width
   * @param {Number} height
   */
  function resizeContent (width, height) {
    var targetElement = document.getElementById('livesync-content');
    targetElement.style.width = width;
    targetElement.style.height = height;
  }

  /**
   * Broadcasts the current viewport dimensions to
   * the server
   * @param {ResizeEvent} evt
   */
  function emitViewportDimensions(evt) {
    var width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
    var height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0)

    connection.emit('resize', {
      width: width,
      height: height
    });
  }

  /**
   * Sets the currently active drawing mode
   * @param {String} drawMode - The drawing mode to change to
   * @returns {Boolean} true if the drawmode could be set
   */
  function setActiveDrawMode(drawMode) {
    if (!DrawingMode[drawMode]) {
      return false;
    }

    currentDrawingMode = DrawingMode[drawMode];
    return true;
  }

  /**
   * Prevents an event from execution
   * @param {Event} evt - Event to block
   */
  function prevent(evt) {
    evt.preventDefault();
    evt.returnValue = false;
  }

  /*********************
   * MIRROR CREATION
   *********************/

  /**
   * Creates a mirror host object
   */
  function createMirrorHost() {
    var targetElement = document.getElementById('livesync-content');

    mirrorClient = new TreeMirrorClient(targetElement, {
      initialize: function(rootId, children) {
        connection.emit('message', {
          f: 'initialize',
          args: [rootId, children]
        });
      },
      applyChanged: function(removed, addedOrMoved, attributes, text) {
        connection.emit('message', {
          f: 'applyChanged',
          args: [removed, addedOrMoved, attributes, text]
        });
      }
    });

    /**
     * ADD MOUSE EVENTS
     */
    targetElement.addEventListener('mousemove', function (evt) {
      var offsets = targetElement.getBoundingClientRect();
      connection.emit('event', {
        type: 'mousemove',
        x: evt.pageX - offsets.left,
        y: evt.pageY - offsets.top
      });
    });

    targetElement.addEventListener('click', function (evt) {
      if (currentDrawingMode !== DrawingMode.POINTER) {
        return false;
      }

      var offsets = targetElement.getBoundingClientRect();
      connection.emit('event', {
        type: 'click',
        x: evt.pageX - offsets.left,
        y: evt.pageY - offsets.top
      });
    });

    targetElement.addEventListener('scroll', function (evt) {
      connection.emit('event', {
        type: 'scroll',
        scrollX: targetElement.scrollLeft,
        scrollY: targetElement.scrollTop
      });
    });
  }

  /**
   * Creates a mirror
   */
  function createMirror() {
    mirrorClient = new TreeMirror(document.getElementById('livesync-content'), {
      createElement: function(tagName) {
        if (tagName === 'SCRIPT') {
          var node = document.createElement('NO-SCRIPT');
          node.style.display = 'none';
          return node;
        }
      }
    });

    [
      'onwheel',
      'onscroll',
      'onmousemove',
      'ontouchmove',
      'onmousewheel',
      'onclick',
      'ondblclick'
    ].map(function(evt) {
      window[evt] = prevent;
    });

    [
      'onkeydown'
    ].map(function(evt) {
      document[evt] = prevent;
    });

    document.body.style.overflow = 'hidden';

    /**
     * Event Listener for Window/Viewport resizing and
     * orientation change events
     */
    window.addEventListener('resize', emitViewportDimensions);
    window.addEventListener('orientationchange', emitViewportDimensions);

    // Initial size sync
    emitViewportDimensions();
  }

  /**
   * Initializes the drawing controls
   */
  function initializeDrawingControls() {
    // Create Draw Button Group
    var drawButtonGroup = document.createElement('div');
    drawButtonGroup.id = 'livesync--drawbuttons';
    drawButtonGroup.className = 'livesync--buttongroup';
    header.appendChild(drawButtonGroup);

    var drawButtonPointer = document.createElement('button');
    drawButtonPointer.className = 'livesync--button livesync--button-active';
    drawButtonPointer.innerHTML = '<i class="fa fa-mouse-pointer"></i>';
    drawButtonPointer.title = 'Mauszeiger';
    drawButtonPointer.setAttribute('draw-mode', 'POINTER')

    var drawButtonRect = document.createElement('button');
    drawButtonRect.className = 'livesync--button';
    drawButtonRect.innerHTML = '<i class="fa fa-square-o"></i>';
    drawButtonRect.title = 'Rechteck';
    drawButtonRect.setAttribute('draw-mode', 'RECT')

    var drawButtonCircle = document.createElement('button');
    drawButtonCircle.className = 'livesync--button';
    drawButtonCircle.innerHTML = '<i class="fa fa-circle-thin"></i>';
    drawButtonCircle.title = 'Kreis';
    drawButtonCircle.setAttribute('draw-mode', 'CIRCLE')

    var drawButtonLine = document.createElement('button');
    drawButtonLine.className = 'livesync--button';
    drawButtonLine.innerHTML = '<i class="fa fa-minus"></i>';
    drawButtonLine.title = 'Linie';
    drawButtonLine.setAttribute('draw-mode', 'LINE')

    var drawButtonArrow = document.createElement('button');
    drawButtonArrow.className = 'livesync--button';
    drawButtonArrow.innerHTML = '<i class="fa fa-long-arrow-right"></i>';
    drawButtonArrow.title = 'Pfeil';
    drawButtonArrow.setAttribute('draw-mode', 'ARROW')

    var buttonGroupElements = [
      drawButtonPointer,
      drawButtonRect,
      drawButtonCircle,
      drawButtonLine,
      drawButtonArrow
    ];

    buttonGroupElements.forEach(function (button) {
      drawButtonGroup.appendChild(button);

      button.addEventListener('click', function (evt) {
        console.log('button clicked');
        buttonGroupElements.forEach(function (button) {
          button.classList.remove('livesync--button-active');
        });
        button.classList.add('livesync--button-active');

        setActiveDrawMode(button.getAttribute('draw-mode'));
      });
    });
  }

  /**
   * Initialize Session Controls
   */
  function initializeSessionControls() {
    var closeSessionButton = document.createElement('div');
    closeSessionButton.innerHTML = '<i class="fa fa-power-off"></i>';
    closeSessionButton.id = 'livesync--close-session';
    closeSessionButton.addEventListener('click', function () {
      connection.disconnect();
      window.close();
    });

    header.appendChild(closeSessionButton);
  }

  /**
   * Initializes the control header for the host
   * @param {String} sessionId - Session ID to display at the top
   */
  function initializeControlHeader(sessionId) {

    var heading = document.createElement('h1');
    heading.className = 'livesync--heading';
    heading.innerHTML = 'LiveSync <small>' + sessionId + '</small>';
    header.appendChild(heading);

    initializeSessionControls();
    initializeDrawingControls();
  }

  /**
   * Connects to the server
   *
   * @param {String} sessionId
   * @param {String} privilegeToken
   * @param {Function} callback
   */
  LiveSync.connect = function (sessionId, privilegeToken, callback) {
    var socketConfig = {
      transports: ['websocket'],
      extraHeaders: {}
    };

    if (privilegeToken) {
      socketConfig.extraHeaders.privilegeToken = privilegeToken;
    }

    header = document.getElementById('livesync-header');
    console.log(header);
    if (header) {
      initializeControlHeader(sessionId);
    }

    /** @type {io} */
    var con = connection = io(location.origin.replace('http', 'ws'), socketConfig);

    con.once('requestSession', function () {
      con.emit('responseSession', {
        privilegeToken: privilegeToken,
        sessionId: sessionId
      }, function (response) {
        if (response && response.accepted) {
          if (privilegeToken) {
            LiveSync.injector(response);

            createMirrorHost();
          } else {
            createMirror();
          }

          con.on('message', onMessage);
          con.on('event', onEvent);

          callback(null, response);
        } else {
          callback(response.error);
        }
      });
    });
  };

  /**
   * Injects the given JS and CSS files
   *
   */
  LiveSync.injector = function (options) {
	   var fileRef;

     // JS Files
     if (options.js && options.js.length > 0) {
       options.js.map(function (file, index) {
         fileRef = document.createElement('script');
         fileRef.setAttribute('type', 'text/javascript');
         fileRef.setAttribute('src', file);
         if (index === options.js.length - 1) {
           fileRef.onload = function () {
             if (typeof bootstrap === 'function') {
               bootstrap();
             }
           }
         }
         document.getElementsByTagName('head')[0].appendChild(fileRef);
       });
     }

     // CSS Files
     if (options.css && options.css.length > 0) {
       options.css.map(function (file) {
         fileRef = document.createElement('link');
         fileRef.setAttribute('rel', 'stylesheet');
         fileRef.setAttribute('type', 'text/css');
         fileRef.setAttribute('href', file);
         document.getElementById('livesync-content').appendChild(fileRef);
       });
     }
  }

  /**
   * Closes the connection
   * @returns {Boolean}
   */
  LiveSync.endSession = function () {
    if (connection) {
      connection.close();
      connection = null;
      return true;
    } else {
      return false;
    }
  }

  /**
   * Requests a new session via POST request from the server
   *
   * @param {Boolean} autoConnect
   * @param {Function} callback
   */
  LiveSync.requestSession = function (options, callback) {
    if (connection !== null) {
      connection.close();
    }

    if (Notification.permission !== 'granted') {
      Notification.requestPermission();
    }

    var request = new XMLHttpRequest();
    request.open('POST', '/live/session', true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    request.onreadystatechange = function (e) {
      if (this.readyState === 4 && this.status === 200) {
        var response = JSON.parse(this.responseText);

        var wnd = window.open('', 'OLB', 'width=300,height=300,menubar=0,location=0,toolbar=0');

        wnd.document.write(
          "<html>" +
            "<head>" +
              "<title>LiveSync</title>" +
              "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>" +
              "<script src='/socket.io/socket.io.js'></script>" +
              "<script src='/live/js/livesync.js'></script>" +
              "<script src='/live/js/mutation-summary.js'></script>" +
              "<script src='/live/js/tree-mirror.js'></script>" +
            "</head>" +
            "<body>" +
              "<div id='livesync-header'></div>" +
              "<div id='livesync-viewport'>" +
                "<div id='livesync-content'></div>" +
              "</div>" +
              "<script>LiveSync.connect('" + response.sessionId + "', '" + response.privilegeToken + "', function(){});</script>" +
            "</body>" +
          "</html>");

        wnd.history.pushState({}, 'LiveSync', 'LiveSync');

        if (typeof callback === 'function') {
          callback(null, response);
        }
      }
    }
    request.send(JSON.stringify(options));
  };

  /**
   * Sends an event over to all connected clients
   */
  LiveSync.sendMessage = function (msg) {
    if (connection) {
      connection.emit('message', msg);
    }
  }

  /**
   * Registers a data handler
   */
  LiveSync.registerMessageHandler = function (messageHandler) {
    if (messageHandlers.indexOf(messageHandler) === -1) {
      messageHandlers.push(messageHandler);
    }
  }

  window.LiveSync = LiveSync;

})(window);