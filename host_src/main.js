require('angular');
require('angular-route');

angular.module('application', ['ngRoute'])
  .config(function($routeProvider) {
    $routeProvider
      .when('/vergleich', {
        templateUrl: '/live/static/templates/main.html',
        controller: 'main'
      })
      .otherwise({
        redirectTo: '/vergleich'
      });
  })
  .controller('main', function ($scope, $http) {
    $scope.parameters = {};
    $scope.offers = [];

    $http.get('/api/requestOffers')
      .then(function(response) {
        if (response.status === 200) {
          $scope.parameters = response.data.parameters;
          $scope.offers = response.data.offers;
        }
      })
  })
  .directive('offer', function() {
	  return {
      restrict: 'E',
      templateUrl: '/live/static/templates/offer.html',
      scope: {
        offer: '='
      },
      link: function(scope, element, attrs) {}
    };
  });

/**
 * Gets called on bootstrapping the application
 */
window.bootstrap = function() {
  var targetNode = document.getElementById('livesync-content');
  //targetNode

  var appNode = document.createElement('div');
  appNode.id = 'application';
  appNode.setAttribute('ng-view', '');
  targetNode.appendChild(appNode);

	angular.bootstrap(appNode, ['application']);
}